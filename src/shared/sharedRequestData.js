import axios from "axios";

export default {
  fetchMovies(category, page) {
    return axios
      .get(
        `https://api.themoviedb.org/3/movie/${category}?api_key=6f98d8dd4c7ca26520eea4fa5134f3e6&page=${page}`
      )
      .then(response => {
        return response.data.results;
      });
  }
};
