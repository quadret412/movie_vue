import Vue from "vue";
import Router from "vue-router";
import Movie from "@/components/Movie";
import Category from "@/components/Category";
import CategoriesList from "@/components/CategoriesList";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "CategoriesList",
      component: CategoriesList,
      props: true
    },
    {
      path: "/category/:api",
      name: "Category",
      component: Category,
      props: true
    },
    {
      path: "/movie/:id",
      name: "movie",
      component: Movie,
      props: true
    }
  ],
  mode: "history"
});
